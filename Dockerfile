FROM debian:buster-slim

RUN apt update
RUN apt install -y openjdk-11-jdk

RUN mkdir /app
COPY 01_base/target/01_base-0.0.1.jar /app

WORKDIR /app

CMD java -jar 01_base-0.0.1.jar
