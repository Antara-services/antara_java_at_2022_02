package ru.antara.generics.bounds;

import ru.antara.model.Animal;
import ru.antara.model.Cat;
import ru.antara.model.HomeCat;
import ru.antara.model.WildCat;

public class GenericsBounded<T extends Cat> {

  public static void main(String[] args) {

    //GenericsBounded<Animal> genericsBounded = new GenericsBounded<>(); //ошибка
    GenericsBounded<Cat> ok1 = new GenericsBounded<>();
    GenericsBounded<HomeCat> ok2 = new GenericsBounded<>();
    GenericsBounded<WildCat> wk = new GenericsBounded<>();
  }

}
